/* Consultas con tablas - Emanuel Moroni 2020 */
/* https://dev.mysql.com/doc/refman/8.0/en/explain.html */

DESCRIBE Artist;
-- Usando tablas
EXPLAIN Artist;
SELECT * FROM Artist;
SELECT Name AS 'Artista' FROM Artist;
SELECT Artist.Name FROM Artist;
SELECT Artist.Name, LENGTH(Artist.Name) FROM Artist;
SELECT COUNT(*) AS 'Total' FROM Artist;

-- Procesando datos
SELECT Artist.Name, LENGTH(Artist.Name) FROM Artist;

SELECT Artist.Name, LENGTH(Artist.Name) AS 'Length'
FROM Artist
WHERE LENGTH(Artist.Name) <= 5
ORDER BY Artist.Name;

SELECT CONCAT("El artista ", Artist.Name, " tiene como ID el numero ", ArtistId) FROM Artist;
