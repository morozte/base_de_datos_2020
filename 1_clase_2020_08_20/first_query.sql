/* Primeras consultas - Emanuel Moroni 2020 */
SELECT 1+1;
SELECT NOW(), UNIX_TIMESTAMP();
SELECT GREATEST(4,5,6,99,8,4,3,12,105);
SELECT 1+1 AS 'Calculo',
    NOW() AS 'Fecha',
    GREATEST(4,5,6,99,8,4,3,12,105) AS 'Maximo';