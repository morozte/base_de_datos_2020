# Trabajar con MySQL y Python - Base de Datos 2020 UTN FRBA ; Emanuel Moroni

# Links de relevancia: 
# - https://www.freecodecamp.org/news/connect-python-with-sql/
# - https://www.w3schools.com/python/default.asp
# - 1. https://towardsdatascience.com/designing-a-relational-database-and-creating-an-entity-relationship-diagram-89c1c19320b2 ; Llegué hasta Making a plan based on the requirements
# - 2. https://towardsdatascience.com/coding-and-implementing-a-relational-database-using-mysql-d9bc69be90f5
# - 3. https://towardsdatascience.com/data-analysis-in-mysql-operators-joins-and-more-in-relational-databases-26c0a968e61e

# Para que importar las librerías sea posible antes debo posicionar en el enviroment que cree con
# anaconda.
import mysql.connector
from mysql.connector import Error
import pandas as pd

# Importo el contenido de un archivo py en el que defini las variables que contienen los strings 
# con los que voy a hacer las querys.
import first_querys

# Variables empleadas para la creación de la conexión servidor
host = "localhost"
user = "root"
contraseña = "password"

# Variables empleadas para la conexión con la base de datos ya existente
db_name = "school"

print("Primer script en Python para conexion con la MySQL Server")

# Empleando la palabra reservada del lenguaje def puedo definir una función.
# 1.a. En este caso estoy creando la función create_server_connection la cual recibe 3 parámetros.
# @params: host_name, en este caso será localhost
# @params: user_name, root
# @params: user_password, password. Este valor es así porque lo cambié, pero por defecto es vacío.
def create_server_connection(host_name, user_name, user_password):
    connection = None
    try:
        connection = mysql.connector.connect(
            host=host_name,
            user=user_name,
            passwd=user_password
        )
        print("MySQL Database connection successful")
    except Error as err:
        print(f"Error: '{err}'")

    return connection

# Empleando la palabra reservada del lenguaje def puedo definir una función.
# 1.b. En este caso estoy creando la función create_server_db_connection la cual recibe 4 parámetros.
# @params: host_name, en este caso será localhost
# @params: user_name, root
# @params: user_password, password. Este valor es así porque lo cambié, pero por defecto es vacío.
# @params: db_name, nombre de la base de datos a la cual me quiero conectar.
def create_db_connection(host_name, user_name, user_password, db_name):
    connection = None
    try:
        connection = mysql.connector.connect(
            host = host_name,
            user = user_name,
            passwd = user_password,
            database = db_name
        )
        print("MySQL Database connection successful")
    except Error as err:
        print(f"Error: '{err}'")

    return connection

# 2. En este caso estoy creando la función create_database la cual recibe 2 parámetros.
# @params: connection, es un objeto connection que voy a obtener una vez establecida la conexión con el servidor
# @params: query, es la query con la voy a crear mi database.
def create_database(connection, query):
    cursor = connection.cursor()
    try:
        cursor.execute(query)
        print("Database created successfully")
    except Error as err:
        print(f"Error: '{err}'")

# 3. Aquí voy a crear la función quien será la encargada de ejecutar las querys que defina.
# Recibe 2 parámetros.
# @params: connection, es un objeto connection que voy a obtener una vez establecida la conexión con el servidor
# @params: query, es la query con la voy a crear mi database.

def execute_query(connection, query):
    cursor = connection.cursor()
    try:
        cursor.execute(query)
        connection.commit()
        print("Query successful")
    except Error as err:
        print(f"Error: '{err}'")


''' 4. Ahora voy a crear una función que me permitirá acceder a la información almacenada en la base de datos 
    sin modificarla. ''' # Aprendí otra forma de comentar XD.

def read_query(connection, query):
    cursor = connection.cursor()
    result = None
    try:
        cursor.execute(query)
        result = cursor.fetchall()
        return result
    except Error as err:
        print(f"Error: '{err}'")

# Empleo las funciones antes definidas.
# Importante, tener presente que emplearé módulos. https://www.w3schools.com/python/python_modules.asp

# Almaceno en conexion el objeto de connection que retorna la función antes definida.
conexion_mysqlserver = create_server_connection(host, user, contraseña)
# Verificado en mi extensión de SQL, estoy creando una nueva BASE DE DATOS llamada school
create_database(conexion_mysqlserver, first_querys.create_query)
# Una vez creada la base de datos me voy a posicionar sobre ella para comenzar a trabajar
conexion_db = create_db_connection(host, user, contraseña, db_name)
# Una vez que estoy empleando la base de datos que cree anteriormente puede comenzar a crear y actualizar contenido
# Descomentar para crear la base de datos. Como ya la cree se encuentra comentado.
'''execute_query(conexion_db, first_querys.create_teacher_table)
execute_query(conexion_db, first_querys.create_participant_table)
execute_query(conexion_db, first_querys.create_client_table)
execute_query(conexion_db, first_querys.create_course_table)
execute_query(conexion_db, first_querys.alter_participant)
execute_query(conexion_db, first_querys.alter_course)
execute_query(conexion_db, first_querys.alter_course_again)
execute_query(conexion_db, first_querys.create_takescourse_table)
execute_query(conexion_db, first_querys.pop_teacher)
execute_query(conexion_db, first_querys.pop_client)
execute_query(conexion_db, first_querys.pop_participant)
execute_query(conexion_db, first_querys.pop_course)
execute_query(conexion_db, first_querys.pop_takescourse)'''

# Lectura de la base de datos empleando una query
results = read_query(conexion_db, first_querys.q1)

# Inicializo una lista vacía; En el tutorial de freeCodeCamp me trabe en la sección de pandas.
from_db = []

for result in results:
  result = result
  from_db.append(result)

print(from_db)