# BASE DE DATOS - 2020 (Emanuel Moroni)
> [Link GitHub Docs - Sintaxis de md](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)  
> [Link GitHub Docs - Sintaxis de md](https://docs.github.com/es/github/writing-on-github/basic-writing-and-formatting-syntax)

## Instalación de Python 3.8 - https://www.python.org/
En la terminal de ubuntu colocar las siguientes líneas para la instalación de python 3.8

```
sudo apt install python3
sudo apt install python3-pip
```
## Instalación de MySQLServer
En la terminal de ubunti colocar las siguientes líneas para la instalación del motor de la base de datos.

```
sudo apt-get install mysql-server
```
Una vez instalado el motor de mi base de datos creo una base de datos de ejemplo a partir de un archivo de consultas SQL. Para hacerlo lo que debo hacer es lo siguiente,

```
sudo mysql < ./chinook_mysql/Chinook_MySql_AutoIncrementPKs.sql
```
> Tener en cuenta que aquí estoy posicionado en una ruta en particular no en root, es por eso que estoy empleando ./ seguido de la dirección de mi archivo .sql.

Una vez creada la base de datos ingreso al motor mysql y puedo mostrar la base de datos creada.

```
sudo mysql
mysql > show databases;
```
El resultado obtenido será una tabla.
```
+--------------------+
| Database           |
+--------------------+
| Chinook            |
| information_schema |
| mysql              |
| performance_schema |
| sys                |
+--------------------+
```
>https://stackoverflow.com/questions/44946270/er-not-supported-auth-mode-mysql-server

Cuando intenté levantar la base de datos desde la extensión de MySQL aquí en VS me topé con el siguiente error: *ERROR: ERROR_NOT_SUPPORTED_AUTH_MODE*. Siguiendo los comentarios que hace el posteo realizado en StackOverflow modifiqué la contraseña del usuario root con el que accedía a MySQL.

Lo importe a recordar aquí es que ahora para ingresar a MySQL tendré que correr la siguiente línea por consola,

```
mysql -u root -p
```

## ¿Cómo comunico el motor de mi base de datos con Python? 
> https://www.freecodecamp.org/news/connect-python-with-sql/
> https://code.visualstudio.com/docs/python/python-tutorial

* ¿Hace falta instalar Anaconda? ¿Qué es?
* Ya pude correr el primer script de python pero debería cambiar el enviroment a Anaconda; Ya instale Anaconda pero ahora debería modificar el enviroment.
* https://phoenixnap.com/kb/how-to-install-anaconda-ubuntu-18-04-or-20-04
* En el video de la primer clase llegue hasta la hora 1:47

Yo necesito comunicar mi enviroment de Python con MySQL Server. Para esto, a continuación lo que haré será instalar un conector para comunicarlos entre si. Esto es la librería de MySQL Server Connector Python Library.

´´´
pip install mysql-connector-python
pip install pandas
´´´