---
output: pdf_document
fontsize: 8pt 
---
![](./img/primer_ERD.jpg)
```
MariaDB [esp_data]> SHOW TABLES;
+--------------------+
| Tables_in_esp_data |
+--------------------+
| Imagen             |
| Maceta             |
| Medicion_Maceta    |
| Medicion_Planta    |
| Seccion            |
+--------------------+
5 rows in set (0.001 sec)

MariaDB [esp_data]> EXPLAIN Imagen;
+--------------+-----------------+------+-----+---------------------+-------------------------------+
| Field        | Type            | Null | Key | Default             | Extra                         |
+--------------+-----------------+------+-----+---------------------+-------------------------------+
| id_imagen    | int(6) unsigned | NO   | PRI | NULL                | auto_increment                |
| nombreImagen | varchar(40)     | NO   |     | NULL                |                               |
| id_medicion  | int(6) unsigned | NO   | MUL | NULL                |                               |
| reading_time | timestamp       | NO   |     | current_timestamp() | on update current_timestamp() |
+--------------+-----------------+------+-----+---------------------+-------------------------------+
4 rows in set (0.004 sec)

MariaDB [esp_data]> EXPLAIN Maceta;
+---------------+-----------------+------+-----+---------+----------------+
| Field         | Type            | Null | Key | Default | Extra          |
+---------------+-----------------+------+-----+---------+----------------+
| id_maceta     | int(6) unsigned | NO   | PRI | NULL    | auto_increment |
| nombre_maceta | varchar(10)     | NO   |     | NULL    |                |
| id_jardin     | int(10)         | YES  |     | NULL    |                |
+---------------+-----------------+------+-----+---------+----------------+
3 rows in set (0.003 sec)

MariaDB [esp_data]> EXPLAIN Medicion_Maceta;
+--------------+-----------------+------+-----+---------------------+-------------------------------+
| Field        | Type            | Null | Key | Default             | Extra                         |
+--------------+-----------------+------+-----+---------------------+-------------------------------+
| id_medicion  | int(6) unsigned | NO   | PRI | NULL                | auto_increment                |
| humAmb       | varchar(10)     | NO   |     | NULL                |                               |
| temp         | varchar(10)     | NO   |     | NULL                |                               |
| presAtm      | varchar(10)     | NO   |     | NULL                |                               |
| id_maceta    | int(6) unsigned | NO   | MUL | NULL                |                               |
| reading_time | timestamp       | NO   |     | current_timestamp() | on update current_timestamp() |
+--------------+-----------------+------+-----+---------------------+-------------------------------+
6 rows in set (0.004 sec)

MariaDB [esp_data]> EXPLAIN Medicion_Planta;
+--------------------+-----------------+------+-----+---------------------+-------------------------------+
| Field              | Type            | Null | Key | Default             | Extra                         |
+--------------------+-----------------+------+-----+---------------------+-------------------------------+
| id_medicion_planta | int(6) unsigned | NO   | PRI | NULL                | auto_increment                |
| humedadSuelo       | varchar(10)     | NO   |     | NULL                |                               |
| iluminancia        | varchar(10)     | NO   |     | NULL                |                               |
| id_seccion         | int(6) unsigned | NO   | MUL | NULL                |                               |
| reading_time       | timestamp       | NO   |     | current_timestamp() | on update current_timestamp() |
+--------------------+-----------------+------+-----+---------------------+-------------------------------+
5 rows in set (0.004 sec)

MariaDB [esp_data]> EXPLAIN Seccion;
+--------------+-----------------+------+-----+---------+----------------+
| Field        | Type            | Null | Key | Default | Extra          |
+--------------+-----------------+------+-----+---------+----------------+
| id_seccion   | int(6) unsigned | NO   | PRI | NULL    | auto_increment |
| nombrePlanta | varchar(30)     | NO   |     | NULL    |                |
| especie      | varchar(30)     | NO   |     | NULL    |                |
| id_maceta    | int(6) unsigned | NO   | MUL | NULL    |                |
+--------------+-----------------+------+-----+---------+----------------+
4 rows in set (0.004 sec)

MariaDB [esp_data]> 


MariaDB [esp_data]> SELECT * FROM Maceta;
+-----------+---------------+-----------+
| id_maceta | nombre_maceta | id_jardin |
+-----------+---------------+-----------+
|         1 | ESP32S_1      |         1 |
|         2 | ESP32S_2      |         2 |
|         3 | ESP32S_3      |         3 |
+-----------+---------------+-----------+
3 rows in set (0.000 sec)

MariaDB [esp_data]> SELECT * FROM Seccion;
+------------+--------------+--------------------+-----------+
| id_seccion | nombrePlanta | especie            | id_maceta |
+------------+--------------+--------------------+-----------+
|          1 | BocaSana     | Cactus Mammillaria |         1 |
|          2 | MicroDasis   | Opuntia            |         1 |
|          3 | Reduncispina | Coryphantha        |         1 |
+------------+--------------+--------------------+-----------+
3 rows in set (0.000 sec)

MariaDB [esp_data]> SELECT * FROM Medicion_Maceta ORDER BY id_medicion DESC LIMIT 10
    -> ;
+-------------+--------+-------+---------+-----------+---------------------+
| id_medicion | humAmb | temp  | presAtm | id_maceta | reading_time        |
+-------------+--------+-------+---------+-----------+---------------------+
|         256 | 45.19  | 28.09 | 1013.46 |         1 | 2020-11-20 19:23:09 |
|         255 | 45.09  | 28.10 | 1013.45 |         1 | 2020-11-20 19:22:05 |
|         254 | 45.06  | 28.08 | 1013.43 |         1 | 2020-11-20 19:21:02 |
|         253 | 44.95  | 28.09 | 1013.41 |         1 | 2020-11-20 19:19:55 |
|         252 | 45.01  | 28.09 | 1013.47 |         1 | 2020-11-20 19:18:51 |
|         251 | 44.84  | 28.12 | 1013.46 |         1 | 2020-11-20 19:17:45 |
|         250 | 45.06  | 28.12 | 1013.48 |         1 | 2020-11-20 19:16:42 |
|         249 | 44.81  | 28.14 | 1013.43 |         1 | 2020-11-20 19:15:38 |
|         248 | 45.11  | 28.13 | 1013.36 |         1 | 2020-11-20 19:14:35 |
|         247 | 44.65  | 28.14 | 1013.48 |         1 | 2020-11-20 19:13:16 |
+-------------+--------+-------+---------+-----------+---------------------+
10 rows in set (0.001 sec)

MariaDB [esp_data]> SELECT * FROM Imagen ORDER BY id_imagen DESC LIMIT 10
    -> ;
+-----------+-----------------------------------+-------------+---------------------+
| id_imagen | nombreImagen                      | id_medicion | reading_time        |
+-----------+-----------------------------------+-------------+---------------------+
|        78 | 2020.11.20_19:23:12_esp32-cam.jpg |         256 | 2020-11-20 19:23:12 |
|        77 | 2020.11.20_19:22:09_esp32-cam.jpg |         255 | 2020-11-20 19:22:09 |
|        76 | 2020.11.20_19:21:05_esp32-cam.jpg |         254 | 2020-11-20 19:21:05 |
|        75 | 2020.11.20_19:19:58_esp32-cam.jpg |         253 | 2020-11-20 19:19:58 |
|        74 | 2020.11.20_19:18:55_esp32-cam.jpg |         252 | 2020-11-20 19:18:55 |
|        73 | 2020.11.20_19:17:51_esp32-cam.jpg |         251 | 2020-11-20 19:17:51 |
|        72 | 2020.11.20_19:16:45_esp32-cam.jpg |         250 | 2020-11-20 19:16:45 |
|        71 | 2020.11.20_19:15:42_esp32-cam.jpg |         249 | 2020-11-20 19:15:42 |
|        70 | 2020.11.20_19:14:39_esp32-cam.jpg |         248 | 2020-11-20 19:14:39 |
|        69 | 2020.11.20_19:13:35_esp32-cam.jpg |         247 | 2020-11-20 19:13:35 |
+-----------+-----------------------------------+-------------+---------------------+
10 rows in set (0.001 sec)

MariaDB [esp_data]> SELECT * FROM Medicion_Planta ORDER BY id_medicion_planta DESC LIMIT 10;
+--------------------+--------------+-------------+------------+---------------------+
| id_medicion_planta | humedadSuelo | iluminancia | id_seccion | reading_time        |
+--------------------+--------------+-------------+------------+---------------------+
|                 45 | 613          | 27          |          1 | 2020-11-20 19:24:09 |
|                 44 | 613          | 27          |          1 | 2020-11-20 19:23:11 |
|                 43 | 613          | 27          |          1 | 2020-11-20 19:22:13 |
|                 42 | 612          | 27          |          1 | 2020-11-20 19:21:15 |
|                 41 | 613          | 27          |          1 | 2020-11-20 19:20:17 |
|                 40 | 612          | 27          |          1 | 2020-11-20 19:19:19 |
|                 39 | 613          | 27          |          1 | 2020-11-20 19:18:21 |
|                 38 | 613          | 27          |          1 | 2020-11-20 19:17:23 |
|                 37 | 613          | 27          |          1 | 2020-11-20 19:16:26 |
|                 36 | 613          | 27          |          1 | 2020-11-20 19:15:28 |
+--------------------+--------------+-------------+------------+---------------------+
10 rows in set (0.001 sec)

MariaDB [esp_data]> 
```