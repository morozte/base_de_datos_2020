# HTTP Request Methods - POST y GET
> https://randomnerdtutorials.com/esp32-http-get-post-arduino/
> https://www.php.net/manual/es/intro-whatis.php

EL protocolo HTTP (Hyper Text Transfer Protocol) es un protocolo de solicitud respuesta entre un 
cliente y un servidor.

* El microcontrolador ESP32 realiza una solicitud HTPP al servidor que se encuentra corriendo en la Raspberry Pi.
* El servidor devuelve una respuesta al cliente. Esta respuesta contiene información del estado de la solicitud y, en algunos casos, también contenido de la solicitud.

## HTTP POST

POST es emepleado para enviar información a un servidor y crear / actualizar un recurso. En el caso de este proyecto es subir la información adquirida de los sensores y la cámara.

La información que será enviada al servidor con POST es almacenada en el cuerpo de la solicitud HTTP,

    POST /update-sensor HTTP/1.1
    Host: example.com
    api_key=api&sensor_name=name&temperature=value1&humidity=value2&pressure=value3
    Content-Type: application/x-www-form-urlencoded

![](/documentacion_img/HTTP-POST-ESP32.png)