# Resumen general - Base de datos 2020
> https://towardsdatascience.com/designing-a-relational-database-and-creating-an-entity-relationship-diagram-89c1c19320b2

Una base de datos relacional es una base de datos que almacena información y provee puntos de acceso a información que está relacionada con otro punto.

Dentro de una base de datos relacional la información es almacenada en tablas en la que cada columna es un atributo y cada fila es un *record*, es decir, un item en particular que tiene esos atributos.

Cada uno de estos *records* dentro de una tabla debe tener un atributo que lo identifique particularmente. Este atributo ID es conocido como **Primary Key**.

También es posible generar una *key* a partir de dos o más atributos persumiendo que ellos identifican a un elemento de la tabla de manera única. Esta key se llama **Composite Key**.

Podemos expresar una relación entre dos tablas diferentes a partir de una clave extrajera o **Foreing Key**.

## Tipos de relaciones entre las entidades de las base de datos
> Entidad:  Es una cosa, objeto, lugar, persona o item de lo que información debe ser almacenada en forma de propiedades, workflow y tablas.

- Relaciones 1-to-N *(One to many)* entre entidades: Por ejemplo, una marca puede tener muchos productos, pero ese producto en particular solo tiene una marca asociada. ¿Qué ocurre si un producto es desarrollado por dos marcas?

- Relaciones 1-to-1 *(One to one)*:

- Relaciones N-to-M *(Many to many)*:

## Identificando las entidades
En principio veo las siguiente entidades posibles de identificar en el proyecto.

1. **Planta**: El "Módulo Planta" será colocado en el periferia de un planta o siembra en particular. Este módulo estará compuesto por un microcontrolador Attiny85, un microcontrolador ESP8266 y dos sensores, uno de humedad de suelo y otro de iluminancia. Esto se hace de esta manera para adquirir información puntual sobre una plantación en particular de la maceta o huerta.

2. **Maceta / Plantación**: El "Módulo Maceta" será colocado en una ubicación general a toda la maceta para poder obtener información de como son las condiciones climáticas, temperatura, humedad ambiente y presión atmosférica, y obtener una foto de como se encuentran las plantas en un momento en particular bajo determinadas condiciones.

3. **Jardín / Invernadero**: Los elementos descriptos anteriormente podrán ser colocados en diferentes partes del jardín para poder tener un control general y específico de como evolucionan los cultivos y plantas y que condiciones son más optimas para el crecimiento particular de cada una de ellas.

## Creación de las tablas a emplear
> https://towardsdatascience.com/coding-and-implementing-a-relational-database-using-mysql-d9bc69be90f5

1. Creación de la tabla Planta

```
CREATE TABLE Planta (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    moduloPlantaId INT NOT NULL,
    especie VARCHAR(30),
    nombre VARCHAR(30),
    humSuelo VARCHAR(10) NOT NULL,
    intLumin VARCHAR(10) NOT NULL,
    MacetaId INT,
    reading_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);
```

2. Creación de la tabla Maceta

```
CREATE TABLE Maceta (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    moduloMacetaId INT,
    humAmb  VARCHAR(10) NOT NULL,
    temp    VARCHAR(10) NOT NULL,
    presAtm VARCHAR(10) NOT NULL,
    jardinId INT NOT NULL,
    reading_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);
```
Por ejemplo, en este caso olvide ingresar una columna perteneciente a la tabla Maceta. Para modificar esta tabla ejecutará la siguiente el siguiente statement de SQL,
```
ALTER TABLE Maceta ADD imgName VARCHAR(20) NOT NULL;
```
Más adelante me di cuenta de que esta columna no me servía de esta manera y que era necesario eliminarla para generar una nueva y diferente.
```
ALTER TABLE Maceta DROP COLUMN imgName;
```

3. Creación de la tabla Imagenes
```
CREATE TABLE Imagenes (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    imagenId VARCHAR(40) NOT NULL,
    reading_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);
```

```
ALTER TABLE Planta ADD FOREIGN KEY (MacetaId) REFERENCES Maceta(moduloMacetaId) ON DELETE SET NULL;
```
### errno: 150 "Foreign key constraint is incorrectly formed"...
> https://mariadb.org/mariadb-innodb-foreign-key-constraint-errors/

Existen diferentes posibilidades de porque este error está apareciendo. En el link anterior se describen las posibles fuentes que generan este error. Corriendo el siguiente statement puedo conocer más información sobre el error obtenido.

```
SHOW WARNINGS;
```
Luego de correrlo, el resultado obtenido es el siguiente:
```
Alter  table `esp_data`.`Planta` with foreign key constraint failed. There is no index in the referenced table where the referenced columns appear as the first columns near 'FOREIGN KEY (MacetaId) REFERENCES Maceta(moduloMacetaId)'.
```
Para conocer más información acerca del error obtenido puedo correr la siguiente línea SQL.
```
SHOW ENGINE INNODB STATUS;
```
Luego de correrlo, el resultado obtenido es el siguiente:
```
------------------------
LATEST FOREIGN KEY ERROR
------------------------
2020-11-08 13:04:42 0x599b43e0 Error in foreign key constraint of table `esp_data`.`Planta`:
FOREIGN KEY (MacetaId) REFERENCES Maceta(moduloMacetaId):
Cannot find an index in the referenced table where the
referenced columns appear as the first columns, or column types
in the table and the referenced table do not match for constraint.
Note that the internal storage type of ENUM and SET changed in
tables created with >= InnoDB-4.1.12, and such columns in old tables
cannot be referenced by such columns in new tables.
Please refer to https://mariadb.com/kb/en/library/foreign-keys/ for correct foreign key definition.
Alter  table `esp_data`.`Planta` with foreign key constraint failed. There is no index in the referenced table where the referenced columns appear as the first columns near 'FOREIGN KEY (MacetaId) REFERENCES Maceta(moduloMacetaId)'.
```
Punto a consultar con el profesor:
- ¿Puede ser que no sea posible generar una relación a partir de una KEY que no sea una PRIMARY KEY?

A partir de la pregunta anterior lo voy a solucionar de la siguiente manera, crearé una nueva tabla en la que almacenaré dos columnas, PRIMARY KEY y un número de módulo asociado a esa PRIMARY KEY.
```
CREATE TABLE tablaMacetasId (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    ESP32SId VARCHAR(10) NOT NULL
);
```
Luego almaceno de forma manual, al menos de manera inicial, tres nuevos elementos en la tabla.
```
INSERT INTO tablaMacetasId (ESP32SId) VALUES ('ESP32S_1');
INSERT INTO tablaMacetasId (ESP32SId) VALUES ('ESP32S_2');
INSERT INTO tablaMacetasId (ESP32SId) VALUES ('ESP32S_3');
```
Luego de repasar que los tipos de variables de datos coincidan, ejecuto nuevamente el statement de SQL para genarar la FOREING KEY y obtengo el siguiente resultado,

```
MariaDB [esp_data]> ALTER TABLE Planta ADD FOREIGN KEY (MacetaId) REFERENCES tablaMacetasId(id) ON DELETE SET NULL;
Query OK, 1221 rows affected (0.196 sec)               
Records: 1221  Duplicates: 0  Warnings: 0
```
Ahora repito lo mismo con la tabla de Maceta y la FOREING KEY de imágenes.
```
ALTER TABLE Maceta ADD FOREIGN KEY (ImagenId) REFERENCES Imagenes(id) ON DELETE SET NULL;
```
# Cambios 20/11/2020
> Estos cambios los realizo a partir de lo repasado en las grabaciones de las clases.

```
CREATE TABLE MacetaP (
    id_maceta INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    ubicacion VARCHAR(10) NOT NULL,
    id_jardin INT NOT NULL
);
```
Recordar que para revisar la estructura empleada en la tabla puedo usar sentencia,
```
EXPLAIN *nombre_de_la_tabla*;
```

Ahora voy a crear una nueva tabla y generar la *FK* para relacionarla con MacetaP,

```
CREATE TABLE Medicion_Maceta (
    id_medicion INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    humAmb  FLOAT NOT NULL,
    temp    FLOAT NOT NULL,
    presAtm FLOAT NOT NULL,
    nombreImagen VARCHAR (30) NOT NULL,
    id_maceta INT NOT NULL DEFAULT 0,
    reading_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);
```
Por último genero la relación con la *FK*,
```
ALTER TABLE MacetaP ADD CONSTRAINT constraint_medicion_id FOREIGN KEY(id_maceta) REFERENCES Medicion_Maceta(id_maceta) ON UPDATE CASCADE ON DELETE CASCADE;
```
```
ALTER TABLE Medicion_Maceta CHANGE id_maceta id_maceta INT(6) UNSIGNED;
```
```
INSERT INTO MacetaP (ubicacion) VALUES ('Derecha');
INSERT INTO MacetaP (ubicacion) VALUES ('Centro');
INSERT INTO MacetaP (ubicacion) VALUES ('Izquierda');
```

¿Es al revés...?
```
ALTER TABLE Medicion_Maceta ADD CONSTRAINT constraint_medicion_id FOREIGN KEY(id_maceta) REFERENCES Maceta(id_maceta) ON UPDATE CASCADE ON DELETE CASCADE;
```
Ahora funcionó. :D

```
CREATE TABLE Imagen (
    id_imagen INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nombreImagen  VARCHAR(40) NOT NULL,
    id_medicion INT(6) UNSIGNED,
    reading_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);
```
```
ALTER TABLE Imagen ADD CONSTRAINT constraint_imagen_id FOREIGN KEY(id_medicion) REFERENCES Medicion_Maceta(id_medicion) ON UPDATE CASCADE ON DELETE CASCADE;
```

```
CREATE TABLE Medicion_Planta (
    id_medicion_planta INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    humedadSuelo  VARCHAR(10) NOT NULL,
    iluminancia VARCHAR(10) NOT NULL,
    id_seccion INT(6) UNSIGNED NOT NULL,
    reading_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);
```

```
CREATE TABLE Seccion (
    id_seccion INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nombrePlanta  VARCHAR(30) NOT NULL,
    especie VARCHAR(30) NOT NULL,
    id_maceta INT(6) UNSIGNED NOT NULL
);
```
```
ALTER TABLE Medicion_Planta ADD CONSTRAINT constraint_medicion_planta_id FOREIGN KEY(id_seccion) REFERENCES Seccion(id_seccion) ON UPDATE CASCADE ON DELETE CASCADE;
```
```
ALTER TABLE Seccion ADD CONSTRAINT constraint_seccion_id FOREIGN KEY(id_maceta) REFERENCES Maceta(id_maceta) ON UPDATE CASCADE ON DELETE CASCADE;
```
# Cambios 10/12/2020

CREARTE TABLE Jardin (
    id_seccion INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nombre_jardin  VARCHAR(30) NOT NULL
);